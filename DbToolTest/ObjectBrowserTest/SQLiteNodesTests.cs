using System;
using NUnit.Framework;
using System.Data;
using Mono.Data.Sqlite;
using System.Data.Common;
using QueryTool;
using System.Collections.Generic;

namespace DbToolTest
{
	[TestFixture]
	public class SQLiteNodesTests
	{
		DbProviderFactory dbFactory = new SqliteFactory();
		private IDbConnection connection;
		
		[SetUp]
		public void Init()
		{
			String constr = "URI=file::memory:,version=3";
			connection = dbFactory.CreateConnection();
			connection.ConnectionString = constr;
			
			connection.Open();
			
			using(IDbCommand command = connection.CreateCommand())
			{
				command.CommandText = "create table testtable(id integer primary key autoincrement, value text)";
				command.ExecuteNonQuery();
				
				command.CommandText  ="insert into testtable(value) values(@val)";
				DbParameter valParam = dbFactory.CreateParameter();
				valParam.ParameterName = "val";
				valParam.DbType = DbType.String;
				command.Parameters.Add(valParam);
				
				for (int i = 0; i < 100; i++)
				{
					valParam.Value = string.Format("test_{0}", i);
					command.ExecuteNonQuery();
				}
				
			}
			
			connection.Close();
		}
		
		[Test]
		public void TestSQLiteTableNode()
		{
			SQLiteTableNode n = new SQLiteTableNode("testtable", this.connection);
			
			List<IObjectBrowserNode> children = n.GetChildren();
			Assert.That(children.Count == 2);
			Assert.That(children[0].Text == "id (integer)");
			Assert.That(children[1].Text == "value (text)");
		}
	}
}

