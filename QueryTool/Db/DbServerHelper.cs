using System;
using System.Collections.Generic;
namespace QueryTool
{
	public class DbServerHelper
	{
		public DbServerHelper ()
		{
		}
		
		public static IList<IDbServer> DbServers
		{
			get
			{
				List<IDbServer> dbServers = new List<IDbServer>();
				dbServers.Add(new SQLiteDbServer());
				
				return dbServers;
			}
		}
	}
}

