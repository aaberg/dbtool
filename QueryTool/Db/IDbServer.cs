using System;
namespace QueryTool
{
	public interface IDbServer : IObjectBrowserNode
	{
		string Name{get;}
		
		string InstanceName{get;}
		
		ConnectorBaseWidget ConnectorWidget{get;}
		
		bool TestConnection(out string errorMsg);
	}
}

