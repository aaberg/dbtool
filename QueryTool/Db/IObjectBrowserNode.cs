using System;
using System.Collections.Generic;
using Gdk;

namespace QueryTool
{
	public interface IObjectBrowserNode
	{
		string Text{get;}
		bool HasChildren{get;}
		List<IObjectBrowserNode> GetChildren();
		bool IsLoaded{get;}
		Pixbuf Icon{get;}
	}
}

