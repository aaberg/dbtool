using System;
using Mono.Data.Sqlite;
using System.Collections.Generic;
using System.IO;
using System.Data;
using Gdk;
namespace QueryTool
{
	public class SQLiteDbServer : IDbServer, IObjectBrowserNode
	{
		public SQLiteDbServer ()
		{
		}
		
		public IDbConnection Connection  {
			get;
			private set;
		}
		
		#region IConnector implementation
		public string Name {
			get {
				return "SQLite";
			}
		}
		
		public string InstanceName{
			get {
				String filename = (ConnectorWidget as SQLiteConnectorWidget).FileName;
				FileInfo fi = new FileInfo(filename);
				return fi.Name;
			}
		}

		private ConnectorBaseWidget _connectorWidget = null;
		public ConnectorBaseWidget ConnectorWidget {
			get {
				if (_connectorWidget == null)
				{
					_connectorWidget = new SQLiteConnectorWidget();
				}
				
				return _connectorWidget;
			}
		}
		
		public bool TestConnection(out String errorMsg)
		{
			String fileName = (this.ConnectorWidget as SQLiteConnectorWidget).FileName;
			String constr = string.Format("URI=file:{0};Version=3", fileName);
			
			this.Connection = new SqliteConnection(constr);
			
			try
			{
				
				using (IDbCommand command = this.Connection.CreateCommand())
				{
					command.CommandText = "select count(*) from sqlite_master";
					this.Connection.Open();
					long result = Convert.ToInt64(command.ExecuteScalar());
					this.Connection.Close();
					errorMsg = String.Empty;
					return result >= 0;
				}
			}
			catch(Exception e)
			{
				this.Connection.Dispose();
				errorMsg = e.Message;
				return false;
			}
		}
		#endregion

		#region IObjectBrowserNode implementation
		private List<IObjectBrowserNode> children = null;
		public List<IObjectBrowserNode> GetChildren ()
		{
			if (children == null)
			{
				children = new List<IObjectBrowserNode>();
				children.Add(new SQLiteTablesNode(this.Connection));
				children.Add (new SQLiteSystemTablesNode(this.Connection));
			}
			return children;
		}

		public string Text {
			get {
				return this.InstanceName;
			}
		}
		
		public bool HasChildren {
			get {
				return true;
			}
		}
		
		public Boolean IsLoaded
		{
			get{
				return children != null;
			}
		}
		
		public Pixbuf Icon
		{
			get 
			{
				return ResourceHelper.DatabaseIcon;
			}
		}
		#endregion
		
		public override string ToString ()
		{
			return Name;
		}
	}
}

 