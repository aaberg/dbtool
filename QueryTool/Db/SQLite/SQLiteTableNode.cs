using System;
using System.Data;
using System.Collections.Generic;
using Gdk;

namespace QueryTool
{
	public class SQLiteTableNode : IObjectBrowserNode
	{
		IDbConnection connection;
		private string tableName;
		private List<IObjectBrowserNode> children = null;
		
		public SQLiteTableNode (String tableName, IDbConnection connection)
		{
			this.tableName = tableName;
			this.Text = tableName;
			this.connection = connection;
			this.IsLoaded = false;
		}

		#region IObjectBrowserNode implementation
		public System.Collections.Generic.List<IObjectBrowserNode> GetChildren ()
		{
			if (this.children == null)
			{
				children = CreateColumnNodes();
				this.IsLoaded = true;
			}
			return children;
		}

		public string Text {
			get; private set;
		}

		public bool HasChildren {
			get {
				return true;
			}
		}

		public bool IsLoaded {
			get;
			private set;
		}
		
		public Pixbuf Icon
		{
			get{return ResourceHelper.TableIcon;}
		}
		#endregion
		
		#region private stuff
		
		private List<IObjectBrowserNode> CreateColumnNodes()
		{
			List<IObjectBrowserNode> columnsList = new List<IObjectBrowserNode>();
			
			connection.Open();
			using (IDbCommand command = connection.CreateCommand())
			{
				command.CommandText = string.Format("select * from {0} limit 1", this.tableName);
				
				using (IDataReader reader = command.ExecuteReader())
				{
					DataTable schemaTable = reader.GetSchemaTable();
					
					DataColumn nameColumn = schemaTable.Columns["ColumnName"];
					DataColumn typeColumn = schemaTable.Columns["DataTypeName"];
					DataColumn isKeyColumn = schemaTable.Columns["IsKey"];
					
					foreach (DataRow row in schemaTable.Rows)
					{
						string colName = Convert.ToString( row[nameColumn] );
						string colType = Convert.ToString( row[typeColumn] );
						bool isKey = Convert.ToBoolean( row[isKeyColumn] );
						columnsList.Add(new SQLiteColumnNode(colName, colType, isKey));
					}
					
				}
			}
			connection.Close();
			
			return columnsList;
		}
		
		#endregion
	}
}

