using System;
using System.Collections.Generic;
using System.Data;
using Gdk;

namespace QueryTool
{
	public class SQLiteSystemTablesNode : IObjectBrowserNode
	{
		private List<IObjectBrowserNode> childs = null;
		private IDbConnection connection;
		
		public SQLiteSystemTablesNode (IDbConnection connection)
		{
			this.connection = connection;
		}

		#region IObjectBrowserNode implementation
		
		public System.Collections.Generic.List<IObjectBrowserNode> GetChildren ()
		{
			if (childs == null)
			{
				childs = new List<IObjectBrowserNode>();
				childs.Add(new SQLiteTableNode("sqlite_master", this.connection));
				if (this.SequenceTableExists())
				{
					childs.Add(new SQLiteTableNode("sqlite_sequence", this.connection));
				}
			}
			return childs;
		}

		public string Text {
			get {
				return "System tables";
			}
		}

		public bool HasChildren {
			get {
				return true;
			}
		}

		public bool IsLoaded {
			get {
				return childs != null;
			}
		}
		
		public Pixbuf Icon
		{
			get{return ResourceHelper.TablesIcon;}
		}
		#endregion
		
		#region private stuff

		private bool SequenceTableExists()
		{
			using (IDbCommand command = this.connection.CreateCommand())
			{
				command.CommandText = "select count(*) from sqlite_master where name = 'sqlite_sequence'";
				this.connection.Open();
				int cnt = Convert.ToInt32(command.ExecuteScalar());
				this.connection.Close();
				
				return cnt == 1;
			}
		}
		
		#endregion
	}
}

