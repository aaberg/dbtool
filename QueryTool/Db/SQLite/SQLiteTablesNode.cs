using System;
using System.Data;
using System.Collections.Generic;
using Gdk;

namespace QueryTool
{
	public class SQLiteTablesNode : IObjectBrowserNode
	{
		private IDbConnection connection;
		
		public SQLiteTablesNode (IDbConnection connection)
		{
			this.connection = connection;
		}

		#region IObjectBrowserNode implementation
		List<IObjectBrowserNode> children = null;
		public System.Collections.Generic.List<IObjectBrowserNode> GetChildren ()
		{
			if (this.children == null)
			{
				this.children = new List<IObjectBrowserNode>();
				using (IDbCommand command = this.connection.CreateCommand())
				{
					try
					{
						connection.Open();
						command.CommandText = "select name from sqlite_master where type='table' and not name = 'sqlite_sequence'";
						using (IDataReader reader = command.ExecuteReader())
						{
							while(reader.Read())
							{
								this.children.Add(new SQLiteTableNode( reader.GetString(0), this.connection ));
							}
						}
						connection.Close();
					}
					catch(Exception e)
					{
						connection.Dispose();
						throw e;
					}
				}
			}
			
			return this.children;
		}

		public string Text {
			get {
				return "Tables";
			}
		}

		public bool HasChildren {
			get {
				return true;
			}
		}
		
		public bool IsLoaded
		{
			get{return children != null;}
		}
		
		public Pixbuf Icon
		{
			get{return ResourceHelper.TablesIcon;}
		}
		#endregion
	}
}

