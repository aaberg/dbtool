using System;
using Gdk;

namespace QueryTool
{
	public class SQLiteColumnNode : IObjectBrowserNode
	{
		private string name;
		private string datatype;
		private bool isKey;
		
		public SQLiteColumnNode (string name, string datatype, bool isKey)
		{
			this.name = name;
			this.datatype = datatype;
			this.isKey = isKey;
		}

		#region IObjectBrowserNode implementation
		public System.Collections.Generic.List<IObjectBrowserNode> GetChildren ()
		{
			throw new NotImplementedException ();
		}

		public string Text {
			get {
				return string.Format("{0} ({1})", this.name, this.datatype);
			}
		}

		public bool HasChildren {
			get {
				return false;
			}
		}

		public bool IsLoaded {
			get {
				throw new NotImplementedException ();
			}
		}
		
		public Pixbuf Icon
		{
			get
			{
				if (isKey)
				{
					return ResourceHelper.Key;
				}
				else 
				{
					return ResourceHelper.ColumnIcon;
				}
			}
		}
		#endregion
	}
}

