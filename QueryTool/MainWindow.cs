using System;
using Gtk;

public partial class MainWindow: Gtk.Window
{	
	
	#region ctor
	public MainWindow (): base (Gtk.WindowType.Toplevel)
	{
		Build ();
		
		// initialize stuff
		ViewObjectBrowserAction_Toggle(ViewObjectBrowserAction, EventArgs.Empty);
	}
	
	#endregion
	
	#region overridden & protected stuff
	
	protected void OnDeleteEvent (object sender, DeleteEventArgs a)
	{
		Application.Quit ();
		a.RetVal = true;
	}

	#endregion
	
	#region event handlers
	
	protected void FileQuitAction_Activate (object sender, EventArgs e)
	{
		Application.Quit();	
	}
	
	protected void ViewObjectBrowserAction_Toggle(object sender, EventArgs e)
	{
		bool show = ((Gtk.ToggleAction)sender).Active;
		
		if (show)
			hpanedLeft.Child1.Show();
		else
			hpanedLeft.Child1.Hide();
	}
	
	#endregion
	
}
