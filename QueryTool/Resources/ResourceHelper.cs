using System;
using Gdk;
using System.IO;

namespace QueryTool
{
	public class ResourceHelper
	{
		public ResourceHelper ()
		{
		}
		
		private static Pixbuf _empty = null;
		public static Pixbuf Empty
		{
			get
			{
				if (_empty == null)
				{
					_empty = new Pixbuf(LoadStream("QueryTool.Resources.Empty.png"));
				}
				return _empty;
			}
		}
		
		private static Pixbuf _databaseIcon = null;
		public static Pixbuf DatabaseIcon
		{
			get
			{
				if (_databaseIcon == null)
				{
					_databaseIcon = new Pixbuf( LoadStream("QueryTool.Resources.database.png"));
				}
				return _databaseIcon;
			}
		}
		
		private static Pixbuf _tablesIcon = null;
		public static Pixbuf TablesIcon
		{
			get
			{
				if (_tablesIcon == null)
				{
					_tablesIcon = new Pixbuf( LoadStream("QueryTool.Resources.table_multiple.png"));
				}
				return _tablesIcon;
			}
		}
		
		private static Pixbuf _tableIcon = null;
		public static Pixbuf TableIcon
		{
			get
			{
				if (_tableIcon == null)
				{
					_tableIcon = new Pixbuf( LoadStream("QueryTool.Resources.table.png"));
				}
				return _tableIcon;
			}
		}
		
		private static Pixbuf _columnIcon = null;
		public static Pixbuf ColumnIcon
		{
			get
			{
				if (_columnIcon == null)
				{
					_columnIcon = new Pixbuf( LoadStream("QueryTool.Resources.bullet_blue.png"));
				}
				return _columnIcon;
			}
		}
		
		private static Pixbuf _key = null;
		public static Pixbuf Key
		{
			get
			{
				if (_key == null)
				{
					_key = new Pixbuf( LoadStream("QueryTool.Resources.key.png"));
				}
				return _key;
			}
		}
		
		private static Stream LoadStream(String name)
		{
			return typeof(ResourceHelper).Assembly.GetManifestResourceStream(name);
		}
	}
}

