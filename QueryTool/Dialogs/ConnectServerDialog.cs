using System;
using Gtk;
namespace QueryTool
{
	public partial class ConnectServerDialog : Gtk.Dialog
	{	
		public ConnectServerDialog ()
		{
			this.Build ();
			
			Gtk.ListStore serversList = new Gtk.ListStore(typeof(string), typeof(IDbServer));
			foreach(IDbServer connector in DbServerHelper.DbServers)
			{
				serversList.AppendValues(connector.Name, connector);
			}
				
			comboboxServers.Model = serversList;
		}
		
		public IDbServer DbServer  {
			get;
			private set;
		}
		
		#region event handlers
		
		protected void comboboxServers_Change(object sender, EventArgs e)
		{
			ComboBox cbox = sender as ComboBox;
			
			//IConnector connector = null;
			TreeIter iter;
			if (cbox.GetActiveIter(out iter))
			{
				this.DbServer = cbox.Model.GetValue(iter, 1) as IDbServer;
				mainBox.PackEnd(this.DbServer.ConnectorWidget);
				mainBox.ShowAll();
			}
			
			
		}
		
		#endregion
	}
}

