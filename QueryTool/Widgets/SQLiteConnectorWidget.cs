using System;
using Gtk;
using System.IO;
namespace QueryTool
{
	[System.ComponentModel.ToolboxItem(true)]
	public partial class SQLiteConnectorWidget : ConnectorBaseWidget
	{
		public SQLiteConnectorWidget ()
		{
			this.Build ();
		}
		
		#region properties
		
		public String FileName{
			get{
				return this.tboxFilename.Text;
//				FileInfo fi = new FileInfo(this.tboxFilename.Text);
//				return fi.Name;
			}
		}
		
		#endregion
		
		#region implemented abstract members of QueryTool.ConnectorBaseWidget
		public override string ConnectionString {
			get {
				return tboxFilename.Text;
			}
		}
		#endregion
		
		#region event handlers
		
		protected void buttonBrowse_Activated(object sender, EventArgs e)
		{
			Gtk.FileChooserDialog dialog = new Gtk.FileChooserDialog(
				"Choose SQLite database",
				(Gtk.Window)Toplevel,
				Gtk.FileChooserAction.Open,
				"Cancel", ResponseType.Cancel,
				"Open", ResponseType.Accept);
			
			dialog.SelectMultiple = false;
			
			dialog.Run();
			tboxFilename.Text = dialog.Filename;
			dialog.Destroy();
		}
		
		#endregion
	}
}

