using System;
using Gtk;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
namespace QueryTool
{
	[System.ComponentModel.ToolboxItem(true)]
	public partial class ObjectBrowser : Gtk.Bin
	{
		public ObjectBrowser ()
		{
			this.Build ();
			
			objectBrowser_disconnectAction.Sensitive = false;
			
			this.DbServers = new ObservableCollection<IDbServer>();
			(this.DbServers as ObservableCollection<IDbServer>).CollectionChanged += HandleDbServersCollectionChanged;
			
			// append column header
			TreeViewColumn treeColumn = new TreeViewColumn();
			treeColumn.Title = "Connected Database Servers";
			
			// add icon renderer
			CellRendererPixbuf iconRenderer = new CellRendererPixbuf();
			treeColumn.PackStart(iconRenderer, false);
			treeColumn.AddAttribute(iconRenderer, "pixbuf", 0);
			
			// add text rendere
			CellRendererText textRenderer = new CellRendererText();
			treeColumn.PackStart(textRenderer, true);
			treeColumn.AddAttribute(textRenderer, "text", 1);
			
			treeviewObjects.AppendColumn(treeColumn);
			
			// create model
			this.Model = new TreeStore(typeof(Gdk.Pixbuf), typeof(string));
			treeviewObjects.Model = this.Model;
			
			
		}

		public TreeStore Model {
			get;
			private set;
		}
		
		#region properties
		
		public IList<IDbServer> DbServers  {
			get;
			private set;
		}
		
		#endregion
		
		#region Event handlers
		
		protected void ObjectBrowser_ConnectAction_Activate(object sender, EventArgs e) 
		{
			ConnectServerDialog serverDialog = new ConnectServerDialog();
			ResponseType respons = (ResponseType)serverDialog.Run();
			
			if (respons == ResponseType.Ok) {
				
				String errorMsg;
				if (serverDialog.DbServer.TestConnection(out errorMsg))
				{
					this.DbServers.Add(serverDialog.DbServer);
				} 
				else
				{
					MessageDialog msgDial = new MessageDialog((Window)this.Toplevel, DialogFlags.Modal, MessageType.Error, ButtonsType.Close, 
					                                          "Error testing connection to database, errormessage:\n{0}", errorMsg);
					msgDial.Run();
					msgDial.Destroy();
				}
			}
			
			serverDialog.Destroy();
		}
		
		void HandleDbServersCollectionChanged (object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
		{
			if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Add) {
				foreach(object o in e.NewItems) {
						
					AddDbServer(o as IDbServer);
				}
			}
		}
		
		protected void HandleRowExpanded (object o, Gtk.RowExpandedArgs args)
		{
			IObjectBrowserNode node = nodes[args.Iter];
			
			if (!node.IsLoaded)
			{
				List<IObjectBrowserNode> childs = node.GetChildren();
				TreeIter dummyIter = dummyIters[args.Iter];
				this.Model.Remove(ref dummyIter);
				dummyIters.Remove(args.Iter);
				foreach(IObjectBrowserNode child in childs)
				{
					AddNode(args.Iter, child);
				}
				this.treeviewObjects.ExpandRow(args.Path, false);
			}
		}
		
		#endregion
		
		#region treeview stuff
		
		Dictionary<TreeIter, TreeIter> dummyIters = new Dictionary<TreeIter, TreeIter>();
		Dictionary<TreeIter, IObjectBrowserNode> nodes = new Dictionary<TreeIter, IObjectBrowserNode>();
		
		private void AddDbServer(IDbServer dbServer)
		{
			AddNode(TreeIter.Zero, dbServer);
		}
		
		private void AddNode(TreeIter parentIter, IObjectBrowserNode node)
		{
			TreeIter nodeIter; 
			if (parentIter.Equals(TreeIter.Zero))
			{
				nodeIter = this.Model.AppendValues(node.Icon, node.Text);
				
			}
			else
			{
				nodeIter = this.Model.AppendValues(parentIter, node.Icon, node.Text);
			}
			this.nodes.Add(nodeIter, node);
			if (node.HasChildren)
			{
				TreeIter dummyIter = this.Model.AppendValues(nodeIter,"Loading...");
				this.dummyIters.Add(nodeIter, dummyIter);
			}
		}


		
		#endregion
		
		
	}
}