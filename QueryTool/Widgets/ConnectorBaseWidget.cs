using System;
namespace QueryTool
{
	public abstract class ConnectorBaseWidget : Gtk.Bin
	{
		public ConnectorBaseWidget ()
		{
		}
		
		public abstract string ConnectionString{get;}
	}
}

